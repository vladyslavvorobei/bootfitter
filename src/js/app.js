import $ from 'jquery';
import Swiper from 'swiper';
import '../../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min';
import '../../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min';
import '../../node_modules/jquery-popup-overlay/jquery.popupoverlay';
import '../../node_modules/jquery-mask-plugin/dist/jquery.mask.min';
import '../../node_modules/jquery-validation/dist/jquery.validate.min';



// --- SCROLL ----
$(document).scroll(function() {
  if($(this).scrollTop() > 70) {
    $('.header').addClass('scroll');
  }
  else {
    $('.header').removeClass('scroll');
  }
});


$(window).ready(function() {



  //---MASONRY-GALLERY----
  var massonryControl = document.getElementsByClassName('gallery__wrapper');
  if (massonryControl.length) {
    var msnry = new Masonry( '.gallery__wrapper', {
      itemSelector: '.gallery__img',
    });
  }


  //---CUSTOM-SCROLL



  // --- REMOVE-DEFAULT-CLICK-IN-BUTTON ----
  $('.button-more').click(function(e) {
    e.preventDefault();
  });

  //----- SWIPER-SLIDER ------
  var swiper = new Swiper('.problem-slider-container', {
    slidesPerView: 3,
    spaceBetween: 30,
    loop: true,
    pagination: {
      el: '.swiper-pagination',
      type: 'fraction',
      renderFraction: function(currentClass, totalClass) {
        return '<span class="' + currentClass + '"></span>' +
          ' из ' +
          '<span class="' + totalClass + '"></span>';
      },
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      991: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
      767: {
        slidesPerView: 1,
        spaceBetween: 20,
      },
    }
  });

  var swiperTwo = new Swiper('.reviews-container', {
    slidesPerView: 3,
    spaceBetween: 30,
    loop: true,
    pagination: {
      el: '.swiper-pagination',
      type: 'fraction',
      renderFraction: function(currentClass, totalClass) {
        return '<span class="' + currentClass + '"></span>' +
          ' из ' +
          '<span class="' + totalClass + '"></span>';
      },
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      1199: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
      767: {
        slidesPerView: 1,
        spaceBetween: 10,
      },
    }
  });
  $('.reviews__text_content').mCustomScrollbar({
    theme: 'minimal-dark'
  });



  var swiperThree = new Swiper('.about-swiper-container', {
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    loop: true,
    spaceBetween: 30,
  });
  //----- SWIPER-SLIDER ------

  //----- FAQ ----
  $('.faq__head').click(function() {
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
    }
    else {
      $('.faq__head').removeClass('active');
      $(this).addClass('active');
    }
    $('.faq__toggle').addClass('.active');
    $(this).next().slideToggle();
    $('.faq__toggle').not($(this).next()).slideUp();
  });
  //-----FAQ-CLOSE -----

  //----- BUTTON-NAV ------

  $('.button-nav').click(function() {
    $('.button-nav').toggleClass('active');
    $('.button-bar').toggleClass('active');
    $('.header__bottom-line').toggleClass('active');
  });

  // --- SWIPER-CARDS --------
  $('.problem-slider__card').click(function() {
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $(this).find('.problem-slider__text').removeClass('active');
      $(this).find('.problem-slider__button-more').removeClass('active');
    } else {
      $('.problem-slider__card').removeClass('active');
      $('.problem-slider__text').removeClass('active');
      $('.problem-slider__button-more').removeClass('active');
      $(this).addClass('active');
      $(this).find('.problem-slider__text').addClass('active');
      $(this).find('.problem-slider__button-more').addClass('active');
    }
  });



  //----- BUTTON-NAV ------


  //Обновляем меню в событии window.resize
  $(window).on('resize', function(e) {
    var parentWidth = $('#nav-bar-filter').parent().width() - 40;
    var ulWidth = $('#more-nav').outerWidth();
    var menuLi = $('#nav-bar-filter > li');
    var liForMoving = new Array();
    //Определим элементы, которые не влезают в меню
    menuLi.each(function() {
      ulWidth += $(this).outerWidth();
      if (ulWidth > parentWidth) {
        liForMoving.push($(this));
      }
    });
    if (liForMoving.length > 0) {
      //Если есть элементы, которые не влезают -> перемещаем их в подменю
      e.preventDefault();
      liForMoving.forEach(function(item) {
        item.clone().appendTo('.subfilter');
        item.remove();
      });
    } else if (ulWidth < parentWidth) { //Проверяем, не нужно ли сдвинуть какие-то элементы обратно
      liForMoving = new Array();
      var moved = $('.subfilter > li');
      for (var i = moved.length - 1; i >= 0; i--) {
        var tmpLi = $(moved[i]).clone();
        tmpLi.appendTo($('#nav-bar-filter'));
        ulWidth += $(moved[i]).outerWidth();
        if (ulWidth < parentWidth) {
          $(moved[i]).remove();
        } else {
          ulWidth -= $(moved[i]).outerWidth();
          tmpLi.remove();
        }
      }
    }
    if ($('.subfilter > li').length > 0) { //Если есть скрытые элементы, показываем блок еще
      $('#more-nav').show();
    } else {
      $('#more-nav').hide();
    }
  });
  $(window).trigger('resize'); //Запустим скрипт при старте

  var mapControl = document.getElementById('map') !== null ? initMap() : null;

  $('.problem-slider__text').mCustomScrollbar({
    theme: 'minimal'
  });


  $('.about__text').mCustomScrollbar({
    theme: 'minimal-dark'
  });

  //---ANCHORN---
  $('.menu a, .subfilter li a').on('click', function(event) {
    var target = $(this.getAttribute('href'));

    if (target.length) {
      event.preventDefault();
      $('html, body').stop().animate({
        scrollTop: target.offset().top - 20
      }, 1000);
    }
  });
  // -- --- ---- MODAL ---- --- --

  $('.modal').popup({
    transition: 'all 0.3s',
    outline: true,
    focusdelay: 400,
    vertical: 'top',
    closebutton: true
  });
  // - - - - JQUERY MASK + VALIDATE - - - -
  $('input[type="tel"]').mask('+7 (000) 000-00-00');
  jQuery.validator.addMethod('phoneno', function(phoneNumber, element) {
    return this.optional(element) || phoneNumber.match(/\+[0-9]{1}\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}/);
  }, 'Введите Ваш телефон');

  $('.form').each(function(index, el) {
    $(el).addClass('form-' + index);

    $('.form-' + index).validate({
      rules: {
        name: 'required',
        tel: {
          required: true,
          phoneno: true
        }
      },
      messages: {
        name: 'Неправильно введенное имя',
        tel: 'Неправильно введен телефон',
      },
      submitHandler: function(form) {
        var t = $('.form-' + index).serialize();
        ajaxSend('.form-' + index, t);
      }
    });
  });
  function ajaxSend(formName, data) {
    jQuery.ajax({
      type: 'POST',
      url: 'rd-mailform.php',
      data: data,
      success: function() {
        $('.modal').popup('hide');
        $('#thanks').popup('show');
        setTimeout(function() {
          $(formName).trigger('reset');
        }, 2000);
      }
    });
  };
});
